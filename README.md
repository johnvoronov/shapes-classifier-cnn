# Shapes Classifier CNN

## LeNet Architecture
For train model `fit.py --architectureType LeNet`

For test model `test.py --architectureType LeNet`


## MiniVGGNet Architecture
For train model `fit.py --architectureType MiniVGGNet`

For test model `test.py --architectureType MiniVGGNet`