batchSize = 32
epochs = 20
width = 28
height = 28
depth = 1
testSize = 0.25
randomState = 42
sgdLearningRate = 0.005
savePath = "./storage/le_net_weights.hdf5"