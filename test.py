from modules.ImageToArrayPreprocessor import ImageToArrayPreprocessor
from modules.SimplePreprocessor import SimplePreprocessor
from modules.DatasetLoader import DatasetLoader

from keras.models import load_model

from config import leNet, miniVGGNet, dataset
from imutils import paths
import numpy as np
import argparse
import cv2


ap = argparse.ArgumentParser()
ap.add_argument("-at", "--architectureType", required=True, help="Type of architecture for fit model")
args = vars(ap.parse_args())


if args["architectureType"] == "LeNet":
    configuration = leNet
elif args["architectureType"] == "MiniVGGNet":
    configuration = miniVGGNet
else:
    raise Exception('An unknown type of architecture for model training.')


print("[INFO] Loading images...")
imagePaths = np.array(list(paths.list_images(dataset.loadPath)))
indexes = np.random.randint(0, len(imagePaths), size=(10,))
imagePaths = imagePaths[indexes]


# Create preprocessors
simplePreprocessor = SimplePreprocessor(configuration.width, configuration.height)
imageToArrayPreprocessor = ImageToArrayPreprocessor()


# Load preprocessors
print("[INFO] Preparing images...")
dataLoader = DatasetLoader(preprocessors=[simplePreprocessor, imageToArrayPreprocessor])
(data, labels) = dataLoader.load(imagePaths)
data = data.astype("float") / 255.0


print("[INFO] Loading model...")
model = load_model(configuration.savePath)


print("[INFO] Evaluating network...")
predictions = model.predict(data, batch_size=32).argmax(axis=1)

n = 0

for (i, imagePath) in enumerate(imagePaths):
    image = cv2.imread(imagePath)

    cv2.putText(
        image,
        "{}".format(
            dataset.classesGroup[predictions[i]]
        ),
        (1, 10),
        cv2.FONT_HERSHEY_SIMPLEX,
        0.5,
        (0, 255, 0),
        2
    )

    cv2.imshow("Image", image)
    cv2.waitKey(0)

    n = n + 3

