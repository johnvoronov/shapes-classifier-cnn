from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelBinarizer

from modules.ImageToArrayPreprocessor import ImageToArrayPreprocessor
from modules.SimplePreprocessor import SimplePreprocessor
from modules.DatasetLoader import DatasetLoader

from architectures.MiniVGGNet import MiniVGGNet
from architectures.LeNet import LeNet

from keras.optimizers import SGD
from keras import backend as K

from config import leNet, miniVGGNet, dataset
import matplotlib.pyplot as plt
from imutils import paths
import numpy as np
import argparse


ap = argparse.ArgumentParser()
ap.add_argument("-at", "--architectureType", required=True, help="Type of architecture for fit model")
args = vars(ap.parse_args())


if args["architectureType"] == "LeNet":
    configuration = leNet

    model = LeNet.build(
        configuration.width,
        configuration.height,
        configuration.depth,
        len(dataset.classesGroup)
    )
elif args["architectureType"] == "MiniVGGNet":
    configuration = miniVGGNet

    model = MiniVGGNet.build(
        configuration.width,
        configuration.height,
        configuration.depth,
        len(dataset.classesGroup)
    )
else:
    raise Exception("An unknown type of architecture for model training.")


print("[INFO] Loading images...")
imagePaths = list(paths.list_images("./dataset/training_set"))


# Create preprocessors
simplePreprocessor = SimplePreprocessor(configuration.width, configuration.height)
imageToArrayPreprocessor = ImageToArrayPreprocessor()


# Load preprocessors
print("[INFO] Preparing images...")
dataLoader = DatasetLoader(preprocessors=[simplePreprocessor, imageToArrayPreprocessor])
(data, labels) = dataLoader.load(imagePaths, verbose=500)


# Reshape images if it needed
if K.image_data_format() == "channels_first":
    data = data.reshape(data.shape[0], configuration.depth, configuration.height, configuration.width)
else:
    data = data.reshape(data.shape[0], configuration.width, configuration.height, configuration.depth)

data = data.astype("float") / 255.0


print("[INFO] Split train and test data...")
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=configuration.testSize, random_state=configuration.randomState)


# Convert the labels from integers to vectors
trainY = LabelBinarizer().fit_transform(trainY)
testY = LabelBinarizer().fit_transform(testY)


print("[INFO] Compiling model...")
optimizationFunc = SGD(lr=configuration.sgdLearningRate)


model.compile(
    loss="categorical_crossentropy",
    optimizer=optimizationFunc,
    metrics=["accuracy"]
)


print("[INFO] Training network...")
History = model.fit(
    trainX,
    trainY,
    validation_data=(testX, testY),
    batch_size=configuration.batchSize,
    epochs=configuration.epochs,
    verbose=1
)


print("[INFO] Serializing network...")
model.save(configuration.savePath)


print("[INFO] Evaluating network...")
predictions = model.predict(testX, batch_size=configuration.batchSize)


print(classification_report(
    testY.argmax(axis=1),
    predictions.argmax(axis=1),
    target_names=dataset.classesGroup
))


plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, configuration.epochs), History.history["loss"], label="train_loss")
plt.plot(np.arange(0, configuration.epochs), History.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, configuration.epochs), History.history["acc"], label="train_acc")
plt.plot(np.arange(0, configuration.epochs), History.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.show()